<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;

    public function files()
    {
        return $this->hasMany('App\Models\File', 'subject_id', 'id')->orderBy("id");
    }
}
