<?php

use App\Admin\Controllers\CategoryController;
use App\Admin\Controllers\FileController;
use App\Admin\Controllers\SubjectController;
use App\Admin\Controllers\UserController;
use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix' => config('admin.route.prefix'),
    'namespace' => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
    'as' => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('users', UserController::class);
    $router->resource('categories', CategoryController::class);
    $router->resource('subjects', SubjectController::class);
    $router->resource('files', FileController::class);
});
