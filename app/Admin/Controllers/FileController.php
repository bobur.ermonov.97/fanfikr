<?php

namespace App\Admin\Controllers;

use App\Models\Category;
use App\Models\File;
use App\Models\Subject;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class FileController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'File';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new File());

        $grid->column('id', __('Id'));
        $grid->column('name_uz', __('Name uz'));
        $grid->column('name_ru', __('Name ru'));
        $grid->column('name_en', __('Name en'));
        $grid->column('price', __('price'))->integer();
        $grid->column('category_id', 'Category')->display(function($categoryId) {
            return Category::find($categoryId)->name_ru;
        });
        $grid->column('subject_id', 'Subject')->display(function($subjectId) {
            return Subject::find($subjectId)->name_ru;
        });
        $grid->column('file', __('file'))->downloadable();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(File::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name_uz', __('Name uz'));
        $show->field('name_ru', __('Name ru'));
        $show->field('name_en', __('Name en'));
        $show->field('price', __('Price'))->number();
        $show->category('Category information', function ($category) {

            $category->setResource('/admin/categories');

            $category->id();
            $category->name_uz();
            $category->name_ru();
            $category->name_en();
        });
        $show->subject('Subject information', function ($subject) {

            $subject->setResource('/admin/subjects');

            $subject->id();
            $subject->name_uz();
            $subject->name_ru();
            $subject->name_en();
        });
        $show->field('file', __('File'))->file();
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new File());

        $form->text('name_uz', __('Name uz'));
        $form->text('name_ru', __('Name ru'));
        $form->text('name_en', __('Name en'));
        $form->number('price', __('Price'));
        $form->select('category_id')->options(Category::all()->pluck('name_ru','id'));
        $form->select('subject_id')->options(Subject::all()->pluck('name_ru','id'));
        $form->file('file', __('File'));

        return $form;
    }
}
