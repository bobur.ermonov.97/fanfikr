<!doctype html>
<html lang="en" >

<head>
    @include('layouts.header')
    @yield('css')

</head>
<body>
<div class="loader"></div>
<div id="layout-wrapper">
    @include('layouts.nav_bar')
{{--    @include('layouts.sidebar')--}}
    @yield('content')
</div>
@include('layouts.footer')
@yield('script')
</body>
</html>
